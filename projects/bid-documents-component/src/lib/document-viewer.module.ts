import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentItemComponent } from './document-item/document-item.component';
import { DocumentViewerComponent } from './document-viewer/document-viewer.component';
import { DocumentHistoryComponent } from './document-history/document-history.component';
import { ModalDialogComponent } from './document-modal-dialog/modal-dialog.component';
import { UploadFilesComponent } from './document-upload-files/upload-files.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { UploadModule } from '@progress/kendo-angular-upload';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { PanelBarModule, LayoutModule } from '@progress/kendo-angular-layout';
import { EditorModule } from '@progress/kendo-angular-editor';
import { KendoUiModule } from './kendo-ui.module';

@NgModule({
  declarations: [
    DocumentItemComponent,
    DocumentViewerComponent,
    DocumentHistoryComponent,
    ModalDialogComponent,
    UploadFilesComponent,
  ],
  imports: [
    CommonModule,
    DialogModule,
    HttpClientModule,
    LayoutModule,
    UploadModule,
    DropDownsModule,
    PanelBarModule,
    ButtonsModule,
    DateInputsModule,
    KendoUiModule,
    FormsModule,
    EditorModule,
    InputsModule,
  ],

  exports: [
    DocumentItemComponent,
    DocumentViewerComponent,
    ModalDialogComponent,
    UploadFilesComponent,
    KendoUiModule,
  ],
})
export class DocumentViewerModule {}
