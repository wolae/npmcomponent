import {
  Component,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  DocumentModel,
  DocumentViewerData,
  DocumentViewerSettings,
  Type,
} from '../models';

import { DialogService } from '@progress/kendo-angular-dialog';
import { UploadFilesComponent } from '../document-upload-files/upload-files.component';

@Component({
  selector: 'bid-document-viewer',
  template: `<section class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="panelbar-wrapper">
            <kendo-panelbar>
              <kendo-panelbar-item
                data-literal="Documents"
                [title]="'Documentos'"
                expanded="true"
              >
                <ng-template kendoPanelBarContent>
                  <bid-document-history
                    [documentCollection]="documentCollectionFiltered"
                    [documentHistoryType]="documentHistoryType"
                    (clickedHistoryItemEmitter)="clickedItem($event)"
                  ></bid-document-history>
                  <div
                    *ngIf="
                      documentCollectionFiltered?.length > 0;
                      else uploadZone
                    "
                    class="d-flex flex-wrap qa-document-selector"
                  >
                    <bid-document-item
                      [documentItem]="doc"
                      [documentRestriction]="documentRestriction"
                      [readonly]="readonly"
                      [disclosureResponse]="disclosureResponse"
                      (removeItemEmitter)="removeItem($event)"
                      (replaceItemEmitter)="replaceItem($event)"
                      (downloadItemEmitter)="downloadItem($event)"
                      *ngFor="
                        let doc of documentCollectionFiltered
                          | slice: 0:documentConfig.limit
                      "
                    ></bid-document-item>
                    <div
                      *ngIf="
                        documentConfig.limit > documentCollectionFiltered.length
                      "
                    >
                      <ng-container
                        *ngTemplateOutlet="uploadZone"
                      ></ng-container>
                    </div>
                  </div>
                </ng-template>
              </kendo-panelbar-item>
            </kendo-panelbar>
          </div>
        </div>
      </div>
    </section>

    <div kendoDialogContainer></div>

    <ng-template #uploadZone>
      <bid-modal-dialog
        [modalTitle]="'Upload Document'"
        [limit]="1"
        [ctaLabel]="'Upload Doument'"
        [documentRestriction]="documentRestriction"
        [readonly]="false"
        [replace]="false"
        (selectedFile)="getSelectedFile($event)"
      ></bid-modal-dialog>
    </ng-template> `,
  styleUrls: ['../styles/styles.scss'],
})
export class DocumentViewerComponent implements OnChanges {
  @Input() documentViewerData: DocumentViewerData;
  @Input() documentViewerSettings: DocumentViewerSettings;
  @Input() translations: string[];
  @Output() uploadActionEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() downloadActionEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() replaceActionEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeActionEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  clickedHistoryItemEmitter: EventEmitter<any> = new EventEmitter<any>();

  expanded = true;
  documentCollectionFiltered: DocumentModel[];
  documentConfig: any;
  documentRestriction: any;
  title: string;
  limit: number;
  readonly: boolean;
  disclosureResponse: string;
  documentHistoryType: Type[];
  replace: boolean = false;
  constructor(readonly modalService: DialogService) {}

  ngOnChanges() {
    this.setDataValues();
    this.setInputSettingsValues();
  }
  setDataValues() {
    this.documentCollectionFiltered = this.documentViewerData.documentCollection;
  }
  setInputSettingsValues() {
    this.documentConfig = this.documentViewerSettings;
    this.documentHistoryType = this.documentViewerSettings.documentHistoryTypesCollection;
    this.disclosureResponse = this.documentViewerSettings.disclosureResponse;
    this.documentRestriction = this.documentViewerSettings.documentRestriction;
    this.limit = this.documentViewerSettings.limit;
    this.readonly = this.documentViewerSettings.readonly;
  }

  clickedItem(id) {
    this.clickedHistoryItemEmitter.emit(id);
  }
  removeItem(event): void {
    this.removeActionEmitter.emit(event);
  }
  replaceItem(id): void {
    //this.replaceActionEmitter.emit(event);
    this.showReplaceComponent(id);
  }
  downloadItem(event): void {
    this.downloadActionEmitter.emit(event);
  }
  getSelectedFile(event) {
    this.uploadActionEmitter.emit(event);
  }
  public showReplaceComponent(id: number) {
    const dialogRef = this.modalService.open({
      title: 'Replace Document',
      // Show component or string message
      content: UploadFilesComponent,
      minWidth: 840,
    });
    const content = dialogRef.content.instance;
    content.limit = this.limit;
    content.documentRestriction = this.documentRestriction;
    content.replace = this.replace;
    dialogRef.result.subscribe(() => {
      this.replaceActionEmitter.emit({ content: content.uploadForm, id });
    });
  }
}
