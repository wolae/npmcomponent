import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { DocumentModel, Type } from '../models';

@Component({
  selector: 'bid-document-history',
  template: `<span
      class="d-inline-block t-margin-right-large t-margin-bottom-xlarge"
      bidCustomTranslate
      literal="'POD Document - QRR Version'"
    ></span>
    <a
      class="cursorLink d-inline-block t-margin-bottom-xlarge t-margin-right-large docClick"
      *ngFor="let item of documentToDownload"
      ><span (click)="clickedItem(item.id)"
        >{{ item?.type }} -
        {{ item?.version === 'NonObjection' ? 'Non-Objection' : item?.version }}
        Version</span
      >
    </a> `,
  styleUrls: ['../styles/styles.scss'],
})
export class DocumentHistoryComponent implements OnChanges {
  @Input() documentCollection: DocumentModel[];
  @Input() documentHistoryType: Type[];
  @Output()
  clickedHistoryItemEmitter: EventEmitter<any> = new EventEmitter<any>();
  public documentToDownload: DocumentModel[];
  constructor() {}

  ngOnChanges(): void {
    this.selectDocumentTypeFromDocumentPackageCollection(
      this.documentCollection
    );
  }
  clickedItem(id): void {
    this.clickedHistoryItemEmitter.emit(id);
  }
  private selectDocumentTypeFromDocumentPackageCollection(
    documentCollection: DocumentModel[]
  ) {
    if (documentCollection.length > 0) {
      documentCollection = documentCollection.filter(
        (doc) => doc.ezshareId !== null
      );
      if (
        this.documentHistoryType[0] !== undefined &&
        this.documentHistoryType[0].length > 0
      ) {
        this.documentToDownload = documentCollection;
      }
    }
  }
}
