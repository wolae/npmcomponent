import { Type } from './type.enum';
import { DocumentModel } from './document.model';

export class DocumentRestriction {
  allowedExtensions: string[];
  maxFileSize: number;
}

export class DocumentViewerData {
  documentCollection: DocumentModel[];
  historyDocumentCollection?: DocumentModel[];
}
export class DocumentViewerSettings {
  documentRestriction: DocumentRestriction;
  documentHistoryTypesCollection?: Type[];
  dateFormat: string;
  limit: number;
  readonly?: boolean;
  expanded?: boolean;
  disclosureResponse?: string;
}
export class DocumentViewerSetup {
  data: DocumentViewerData;
  settings: DocumentViewerSettings;
  error: string[];
}
