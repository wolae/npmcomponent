export enum Type {
    MainDocument = 'MainDocument',
    PodDocument = 'PodDocument',
    MinutesMainDocument = 'MinutesMainDocument',
    Minutes = 'Minutes',
    DraftLegalResolution = 'DraftLegalResolution',
    LegalResolution = 'LegalResolution'
}
