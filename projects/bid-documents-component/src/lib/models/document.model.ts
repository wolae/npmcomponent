export class DocumentModel {
  uuid: string;
  tempUuid: string;
  tempName: string;
  name: string;
  ezshareId?: string;
  type: string;
  version: string;
  user: string;
  uploadedOn: Date;
  urlTemp: string;
  id?: number;
  created: Date;
  createdBy: string;
}
