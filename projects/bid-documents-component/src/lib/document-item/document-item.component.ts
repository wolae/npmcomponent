import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from '@angular/core';

import { FileRestrictions } from '@progress/kendo-angular-upload';
import { DocumentModel } from '../models';

@Component({
  selector: 'bid-document-item',
  template: `<kendo-card
    [class]="cardClass"
    *ngIf="documentItem"
    [attr.id]="documentItem.id ? documentItem.id : ''"
  >
    <kendo-dropdownbutton
      class="k-card__button"
      [data]="itemMenu"
      [textField]="'actionName'"
      [popupSettings]="{ align: 'right', popupClass: popupClass }"
    >
      <span class="fas fa-ellipsis-h"></span>
    </kendo-dropdownbutton>
    <kendo-card-header>
      <figure class="ic-sprites pdf"></figure>
      <div class="card-header-info">
        <span class="card-label">ID NUMBER</span>
        <a class="card-ref qa-ezshare">{{
          documentItem.ezshareId ? documentItem.ezshareId : ''
        }}</a>
      </div>
    </kendo-card-header>
    <kendo-card-body>
      <div class="k-hbox">
        <div class="k-column k-text-left">
          <span class="card-label">Document Name</span>
          <ng-template #tooltipTemplate let-anchor>
            {{ anchor.nativeElement.getAttribute('data-title') }}
          </ng-template>
          <div kendoTooltip>
            <p
              class="k-card__doc-name"
              data-title="{{ documentItem.name || '-' }}"
            >
              {{ documentItem.name || '-' }}
            </p>
          </div>
        </div>
      </div>

      <div class="k-hbox">
        <div class="k-column k-text-left k-card__doc-type">
          <span class="card-label">Type</span>
          <ng-template #tooltipTemplate let-anchor>
            {{ anchor.nativeElement.getAttribute('data-title') }}
          </ng-template>
          <div kendoTooltip>
            <p
              class="txt txt--bold t-margin-bottom-none"
              data-title="{{ documentItem.type }}"
            >
              {{ documentItem.type }}
            </p>
          </div>
        </div>
        <div class="k-column k-text-left k-card__doc-version">
          <span class="card-label">Version</span>
          <ng-template #tooltipTemplate let-anchor>
            {{ anchor.nativeElement.getAttribute('data-title') }}
          </ng-template>
          <div kendoTooltip>
            <p
              class="txt txt--bold t-margin-bottom-none"
              data-title="{{ documentItem.version }}"
            >
              {{ documentItem.version }}
            </p>
          </div>
        </div>
      </div>
    </kendo-card-body>
    <kendo-card-footer [ngSwitch]="disclosureResponse">
      <div *ngSwitchDefault class="k-hbox">
        <div class="k-column k-text-left">
          <span class="card-label">User</span>
          <ng-template #tooltipTemplate2 let-anchor>
            {{ anchor.nativeElement.getAttribute('data-title') }}
          </ng-template>

          <div kendoTooltip>
            <p
              class="k-card__user-name"
              data-title="{{ documentItem.createdBy }}"
            >
              {{ documentItem.createdBy }}
            </p>
          </div>
        </div>
        <div class="k-column k-text-left">
          <span class="card-label">Uploaded On</span>
          <p class="txt t-margin-bottom-none">
            {{ documentItem.created | date: formatDate }}
          </p>
        </div>
      </div>
      <div *ngSwitchCase="'SentToPublisher'" class="k-hbox">
        <span class="far fa-paper-plane k-card__icon"></span>
        <div>
          <span class="card-label">Sent To Publisher</span>
          <span class="card-info">Uploaded On</span>
          <span class="card-info">
            {{ documentItem.created | date: formatDate }}
          </span>
        </div>
      </div>
      <div *ngSwitchCase="'Disclosed'" class="k-hbox">
        <span class="fas fa-check k-card__icon"></span>
        <div>
          <span class="card-label">Disclosed</span>
          <span class="card-info">Uploaded On</span>
          <span class="card-info">
            {{ documentItem.created | date: formatDate }}
          </span>
        </div>
      </div>
      <div *ngSwitchCase="'DisclosureRejected'" class="k-hbox">
        <span class="far fa-times k-card__icon"></span>
        <div>
          <span class="card-label">Disclosure Rejected</span>
          <span class="card-info">Uploaded On</span>
          <span class="card-info">
            {{ documentItem.created | date: formatDate }}
          </span>
        </div>
      </div>
      <div *ngSwitchCase="'Undisclosed'" class="k-hbox">
        <span class="far fa-times k-card__icon"></span>
        <div>
          <span class="card-label">Undisclosed</span>
          <span class="card-info">Uploaded On</span>
          <span class="card-info">
            {{ documentItem.created | date: formatDate }}
          </span>
        </div>
      </div>
      <div *ngSwitchCase="'DisclosureFailed'" class="k-hbox">
        <span class="far fa-times k-card__icon"></span>
        <div>
          <span class="card-label">Disclosure Failed</span>
          <span class="card-info">Uploaded On</span>
          <span class="card-info">
            {{ documentItem.created | date: formatDate }}
          </span>
        </div>
      </div>
    </kendo-card-footer>
  </kendo-card> `,
  styleUrls: ['../styles/styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DocumentItemComponent implements OnInit {
  @Input() documentItem: DocumentModel;
  @Input() documentRestriction: FileRestrictions;
  @Input() formatDate: string;
  @Input() readonly: boolean;
  @Input() disclosureResponse: string;
  @Output() downloadItemEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() replaceItemEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeItemEmitter: EventEmitter<any> = new EventEmitter<any>();

  public cardClass: string;
  public popupClass = 'k-popup--document';
  public itemMenu: Array<any>;

  constructor() {}

  ngOnInit(): void {
    this.checkDiscloseResponse();

    this.initItemMenu();
  }
  initValues() {}

  public checkDiscloseResponse() {
    if (this.disclosureResponse === 'Disclosed') {
      this.cardClass = 'k-card--document k-card--disclosed';
    } else {
      if (this.disclosureResponse === 'SentToPublisher') {
        this.cardClass = 'k-card--document k-card--sent';
      } else {
        if (
          this.disclosureResponse === 'DisclosureFailed' ||
          this.disclosureResponse === 'Undisclosed' ||
          this.disclosureResponse === 'DisclosureRejected'
        ) {
          this.cardClass = 'k-card--document k-card--undisclosed';
        } else {
          this.cardClass = 'k-card--document ';
        }
      }
    }
  }

  /* ngOnChanges(changes: { [propKey: string]: SimpleChange }): void {
      this.changeItem(changes.documentItem);
  } */

  public downloadAction(): void {
    this.downloadItemEmitter.emit(this.documentItem.id);
  }

  public replaceAction(): void {
    this.replaceItemEmitter.emit(this.documentItem.id);
  }

  public removeAction(): void {
    this.removeItemEmitter.emit(this.documentItem.id);
  }

  public initItemMenu(): void {
    this.itemMenu = [
      {
        actionName: 'Download',
        iconClass: 'far fa-download popup-icon',
        click: () => this.downloadAction(),
      },
    ];
    if (
      !this.readonly &&
      this.disclosureResponse !== 'SentToPublisher' &&
      this.disclosureResponse !== 'Disclosed'
    ) {
      this.itemMenu.push(
        {
          actionName: 'Replace',
          click: () => this.replaceAction(),
          iconClass: 'far fa-exchange popup-icon',
        },
        {
          actionName: 'Delete',
          click: () => this.removeAction(),
          iconClass: 'far fa-trash-alt popup-icon',
        }
      );
    }
  }
}
