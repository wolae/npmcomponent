import { UploadFilesComponent } from './../document-upload-files/upload-files.component';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { FileRestrictions } from '@progress/kendo-angular-upload';
import { FormGroup } from '@angular/forms';
import { DialogService } from '@progress/kendo-angular-dialog';
@Component({
  selector: 'bid-modal-dialog',
  template: `<div class="wrapper d-flex">
      <button
        data-button-selector="Upload Document"
        [disabled]="readonly"
        (click)="showUploadComponent()"
        class="k-button k-button--document"
      >
        <span class="far fa-arrow-to-top btn-icon"></span>
        <span class="btn-label">{{ ctaLabel }}</span>
      </button>
    </div>
    <ng-container #container></ng-container>`,
  styleUrls: ['../styles/styles.scss'],
})
export class ModalDialogComponent implements OnInit {
  @Input() modalTitle: string;
  @Input() limit: number;
  @Input() documentRestriction: FileRestrictions;
  @Input() readonly: boolean;
  @Input() ctaLabel: string;
  @Input() replace: boolean;
  @Output() selectedFile = new EventEmitter<FormGroup>();

  constructor(readonly modalService: DialogService) {}

  ngOnInit(): void {}

  public showUploadComponent() {
    const dialogRef = this.modalService.open({
      title: this.modalTitle,
      // Show component or string message
      content: UploadFilesComponent,
      minWidth: 840,
    });
    const content = dialogRef.content.instance;
    content.limit = this.limit;
    content.documentRestriction = this.documentRestriction;
    content.replace = this.replace;
    dialogRef.result.subscribe(() => {
      this.selectedFile.emit(content.uploadForm);
    });
  }
}
