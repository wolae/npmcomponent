import {
  Component,
  Input,
  AfterContentChecked,
  OnInit,
  OnDestroy,
  EventEmitter,
  Output,
} from '@angular/core';
import { FileRestrictions } from '@progress/kendo-angular-upload';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ReactiveFormsModule,
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { DialogRef } from '@progress/kendo-angular-dialog';
@Component({
  selector: 'bid-upload-files',
  template: `<form
      data-dialog-selector="Upload document"
      [formGroup]="uploadForm"
      *ngIf="!loader; else skeleton"
      novalidate
      (ngSubmit)="save(uploadForm.get('files').value, uploadForm.valid)"
    >
      <p class="txt--large txt--bold t-margin-bottom-huge">
        Select the file and upload.
      </p>
      <kendo-fileselect
        formControlName="files"
        [multiple]="isMultiple"
        [restrictions]="documentRestriction"
        class="k-upload--document"
        [accept]="'application/pdf'"
        style="
      --before-content: 'Drag an drop';
      --new-content: ' here';
      --after-content: 'or';
      --ext-content: '{{ documentRestriction?.allowedExtensions }}';
      --select-button: 'SELECT PDF';"
      >
        <kendo-upload-messages
          [dropFilesHere]="' here'"
          [invalidFileExtension]="
            'The document selected is not a valid file format. Please select .pdf and try again.'
          "
        >
        </kendo-upload-messages>
      </kendo-fileselect>

      <div class="modal__footer">
        <button kendoButton (click)="onCancelAction()" class="btn btn-link">
          Cancel
        </button>
        <button
          kendoButton
          [disabled]="!uploadForm.valid"
          data-cy="uploadFile"
          type="submit"
          primary="true"
          trackerName="Upload document"
        >
          Upload Document
        </button>
      </div>
    </form>
    <!-- SKELETON -->
    <ng-template #skeleton>
      <!-- <bid-skeleton blocks="1"></bid-skeleton> -->
    </ng-template> `,
  styleUrls: ['../styles/styles.scss'],
})
export class UploadFilesComponent
  implements AfterContentChecked, OnInit, OnDestroy {
  @Input() limit: number;
  @Input() documentRestriction: FileRestrictions;
  @Input() replace: boolean;
  @Output() selectedFile = new EventEmitter<FormGroup>();

  public uploadForm: FormGroup;
  public myFiles: Array<any>;
  public submitted = false;
  public isMultiple: boolean;
  public prevContent: string;
  public newContent: string;
  public nextContent: string;
  public loader;

  readonly subscriptionsCollection: Subscription[] = [];

  constructor(public dialog: DialogRef, readonly fb: FormBuilder) {
    this.uploadForm = this.fb.group({
      files: [this.myFiles, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.loader = false;
  }

  ngAfterContentChecked(): void {
    this.checkLimit();
  }

  public onCancelAction(): void {
    this.dialog.close(false);
  }

  public save(): void {
    this.selectedFile.emit(this.uploadForm);
    this.dialog.close(false);
  }

  public getProgress(progress: number): boolean {
    return progress === 100;
  }

  private checkLimit(): void {
    this.isMultiple = this.limit > 1;
  }

  private unsubscribeAll(): void {
    this.subscriptionsCollection.forEach((sub) => sub.unsubscribe);
  }

  ngOnDestroy(): void {
    this.unsubscribeAll();
  }
}
