import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Kendo
import { DialogModule, DialogsModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule, ButtonModule } from '@progress/kendo-angular-buttons';
import { UploadsModule } from '@progress/kendo-angular-upload';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DropDownListModule } from '@progress/kendo-angular-dropdowns';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { PanelBarModule } from '@progress/kendo-angular-layout';

@NgModule({
  imports: [
    CommonModule,
    DialogModule,
    ButtonsModule,
    ButtonModule,
    DialogsModule,
    UploadsModule,
    DropDownListModule,
    DateInputsModule,
    InputsModule,
    PanelBarModule,
    TooltipModule,
  ],
  exports: [
    CommonModule,
    DialogModule,
    ButtonsModule,
    ButtonModule,
    DialogsModule,
    UploadsModule,
    DropDownListModule,
    DateInputsModule,
    InputsModule,
    PanelBarModule,
    TooltipModule,
  ],
})
export class KendoUiModule {}
