/*
 * Public API Surface of bid-documents-component
 */

export * from './lib/document-viewer/document-viewer.component';
export * from './lib/document-history/document-history.component';
export * from './lib/document-item/document-item.component';
export * from './lib/document-modal-dialog/modal-dialog.component';
export * from './lib/document-upload-files/upload-files.component';
export * from './lib/document-viewer.module';
